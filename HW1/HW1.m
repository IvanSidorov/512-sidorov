%% Home work 1


%% Problem 1
%
X = [1, 1.5, 3.5, 4.78, 5, 7.2, 9, 10, 10.1, 10.56];
Y1 = -2 + .5*X;
Y2 = -2 + .5*X.^2;
plot(X,Y1,X,Y2);
%To make plot look better
title('Problem 1');
xlabel('X');
ylabel('Y1 and Y2');
%% Problem 2
X=linspace(-20,40,120);
Answer = sum(X)
%% Problem 3
A = [3 4 5;
   2 12 6;
   1 7 4];
b = [3; -2; 10];
C = A'*b
D = (A'*A)\b
E = sum(A,2)'*b
F = A([1 3],1:2)
% linear equation solution
Sol=A\b
%% Problem 4
B=blkdiag(A,A,A,A,A);
%% Problen 5
A = random('Normal',6.5,3,5,3);
disp(A);
disp(A>=4);
%% Problem 6
% Here I assume that the CVS file is already in the working directory
data = readtable('datahw1.csv');
% I give variables meaningful names
data.Properties.VariableNames = {'firm_id','year','Export','RD','prod','cap'};
lm=fitlm(data,'prod ~ Export + RD + cap');
% that counts, but data had missing values that were droped by fitlm authomatically. Next time try to not use toolbox for very simple stuff like regressions
% I display only point estimates (without standard errors or pValues)
disp(lm.Coefficients(:,1));



