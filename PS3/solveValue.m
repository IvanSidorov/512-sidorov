function [V,p,iter] = solveValue(v0, p0)

global L v CRIT lambda Maxiter;

V = v0;
p = p0;
check = 1;
iter = 0;
nV = zeros(L,L);
np = zeros(L,L);

while check > CRIT
    for w1=1:L
        for w2=1:L
            fun = @(x) -maximand(x,p(w2,w1),V,w1,w2);
            [np(w1,w2),nV(w1,w2)] = fminbnd(fun,-2*v,2*v);
        end
    end   
    
    check = max( max(max(abs((-nV-V)./(1+abs(nV))))),  max(max(abs((np-p)./(1+np)))));
    %disp(check);
    V = -lambda * nV + (1-lambda)*V;
    p = lambda * np + (1-lambda)*p;
    iter = iter+1;
    check=min(Maxiter-iter,check);
end