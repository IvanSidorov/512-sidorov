
clear;
global L  v k l ro delta beta Maxiter CRIT lambda;

L = 30;
v = 10;
k=10;
l=15;
ro=0.85;
delta = 0.03;
beta = 1/1.05;
CRIT = 10^(-5); 
lambda = 0.85 % Dampening
Maxiter=1500;

