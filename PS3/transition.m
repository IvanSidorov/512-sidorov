function T = transition(p)
global  L v delta;
M=zeros(L*L,L*L);
for w1=1:L
    
    for w2=1:L
        t=w1+(w2-1)*L;
        D0=1/(1+exp(v-p(w1,w2))+exp(v-p(w2,w1)));
        D1=exp(v-p(w1,w2))*D0;
        D2=exp(v-p(w2,w1))*D0;
        prdd=(1-(1-delta)^w1)*(1-(1-delta)^w2);
        prds=(1-(1-delta)^w1)*((1-delta)^w2);
        prsd=((1-delta)^w1)*(1-(1-delta)^w2);
        prss=((1-delta)^w1)*((1-delta)^w2);
        if w1>1 & w1<L & w2>1 & w2<L
            M(t,t)=D0*prss+D1*prds+D2*prsd;
            M(t,t+1)=D1*prss;
            M(t,t-1)=D0*prds+D2*prdd;
            M(t,t+L)=D2*prss;
            M(t,t-L)=D0*prsd+D1*prdd;
            M(t,t-1-L)=D0*prdd;
            M(t,t-1+L)=D2*prds;
            M(t,t+1-L)=D1*prsd;
        elseif w1==1 & w2==1
            M(t,t)=D0*prss+D1*prds+D2*prsd+D0*prds+D2*prdd+D0*prsd+D1*prdd+D0*prdd;
            M(t,t+1)=D1*prss+D1*prsd;
            M(t,t+L)=D2*prss+D2*prds;
        elseif w1==1 & w2==L
            M(t,t)=D0*prss+D1*prds+D2*prsd+D0*prds+D2*prdd+D2*prss+D2*prds;
            M(t,t+1)=D1*prss;
            M(t,t-L)=D0*prsd+D1*prdd+D0*prdd;            
            M(t,t+1-L)=D1*prsd;
        elseif w1==L & w2==1
            M(t,t)=D0*prss+D1*prds+D2*prsd+D1*prss+D0*prsd+D1*prdd+D1*prsd;            
            M(t,t-1)=D0*prds+D2*prdd+D0*prdd;
            M(t,t+L)=D2*prss;            
            M(t,t-1+L)=D2*prds;            
        elseif w1==L & w2==L
            M(t,t)=D0*prss+D1*prds+D2*prsd+D1*prss+D2*prss;            
            M(t,t-1)=D0*prds+D2*prdd+D2*prds;            
            M(t,t-L)=D0*prsd+D1*prdd+D1*prsd;
            M(t,t-1-L)=D0*prdd;            
        elseif w1>1 & w1<L & w2==L
            M(t,t)=D0*prss+D1*prds+D2*prsd+D2*prss;
            M(t,t+1)=D1*prss;
            M(t,t-1)=D0*prds+D2*prdd+D2*prds;            
            M(t,t-L)=D0*prsd+D1*prdd;
            M(t,t-1-L)=D0*prdd;            
            M(t,t+1-L)=D1*prsd;
        elseif w1>1 & w1<L & w2==1
            M(t,t)=D0*prss+D1*prds+D2*prsd+D0*prsd+D1*prdd;
            M(t,t+1)=D1*prss+D1*prsd;
            M(t,t-1)=D0*prds+D2*prdd+D0*prdd;
            M(t,t+L)=D2*prss;
            M(t,t-1+L)=D2*prds;            
        elseif w1==L & w2>1 & w2<L
            M(t,t)=D0*prss+D1*prds+D2*prsd+D1*prss;            
            M(t,t-1)=D0*prds+D2*prdd;
            M(t,t+L)=D2*prss;
            M(t,t-L)=D0*prsd+D1*prdd+D1*prsd;
            M(t,t-1-L)=D0*prdd;
            M(t,t-1+L)=D2*prds;            
        elseif w1==1 & w2>1 & w2<L
            M(t,t)=D0*prss+D1*prds+D2*prsd+D0*prds+D2*prdd;
            M(t,t+1)=D1*prss;            
            M(t,t+L)=D2*prss+D2*prds;
            M(t,t-L)=D0*prsd+D1*prdd+D0*prdd;            
            M(t,t+1-L)=D1*prsd;
        end
    end
end





T=M;