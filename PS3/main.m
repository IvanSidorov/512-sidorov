clear;
tic;
setupParams;

V0 =repmat(1:L, L, 1)'; %initial value for value function.
p0 = 7*ones(L,L);
[Va, pa, ita] = solveValue(V0, p0); % value function iteration
T=transition(pa); % L*L by L*L transition matrix based on policy function
start= [1,zeros(1,L*L-1)];
T10=T^10;
dist10=start*T10;
dist20= dist10*T10;
dist30= dist20*T10;
dists=dist30*T10^100; %stationary distribution
stationarity_check = max(abs(dists-dists*T)) %check of the stationary distribution
elapsed = toc./60;
disp(sprintf('Elapsed time: %12.4f minutes', elapsed));
%% Plots
figure(1);
mesh(Va);
title('Value Function');

figure(2);
mesh(pa);
title('Policy(price) Function');

figure(3);
mesh(reshape(dist10,L,L));
title('Distribution after 10 periods');

figure(4);
mesh(reshape(dist20,L,L));
title('Distribution after 20 periods');

figure(5);
mesh(reshape(dist30,L,L));
title('Distribution after 30 periods');

figure(6);
mesh(reshape(dists,L,L));
title('Stationary Distribution');

csvwrite('value.csv',Va);
csvwrite('policy.csv',pa);
csvwrite('stdist.csv',reshape(dists,L,L));


