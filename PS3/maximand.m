function z = maximand(p1,p2,V,w1,w2)

global v beta k l ro delta L;
exp1=exp(v-p1);
exp2=exp(v-p2);
D0=1/(1+exp1+exp2);
D1=exp1*D0;
D2=exp2*D0;
c=k*(min(w1,l)^(log(ro)/log(2)));
prdd=(1-(1-delta)^w1)*(1-(1-delta)^w2);
prds=(1-(1-delta)^w1)*((1-delta)^w2);
prsd=((1-delta)^w1)*(1-(1-delta)^w2);
prss=((1-delta)^w1)*((1-delta)^w2);
w1m=max(1,w1-1);
w2m=max(1,w2-1);
w1p=min(L,w1+1);
w2p=min(L,w2+1);
W0=V(w1,w2)*prss+V(w1,w2m)*prsd+V(w1m,w2)*prds+V(w1m,w2m)*prdd;
W1=V(w1p,w2)*prss+V(w1p,w2m)*prsd+V(w1,w2)*prds+V(w1,w2m)*prdd;
W2=V(w1,w2p)*prss+V(w1,w2)*prsd+V(w1m,w2p)*prds+V(w1m,w2)*prdd;
z = D1*(p1-c)+beta*(D0*W0+D1*W1+D2*W2);
