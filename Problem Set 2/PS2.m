% My programme is based on stochgrow.m 
% looks like my code for answer key. I cannot add anything, well done
clear all;
close all;

% Parameter values

beta=0.95; % discount factor
Z=21;              % grid number for shocks
%Z=5
sigma=0.1;       % std of error term in AR(1) process
mu=0.5;           % mean of the error term 
ro=0.5;       % AR(1) parameter 
 
 


N=1001; % grid number for the stock 
khigh=100;
klow=0;
k=klow:(khigh-klow)/(N-1):khigh;


% GRID FOR PRODUCTIVITY SHOCKS

[prob,grid]=tauchen(Z,mu,ro,sigma);

% VALUE FUNCTION ITERATION

vinitial=zeros(N,Z);
vrevised=zeros(N,Z);
decision=zeros(N,Z);
  
ONE=ones(N,1);
 
%iteration

diff=1;

while diff>0.001
    
    Ev=vinitial*prob';   % find the expected value of value function
      
    for i=1:N           % for each k, find the optimal decision rule
      x=k(i)*ONE-k';
      xplus=max(x,zeros(size(x))); % some harvests are negative. I make them zero. Later I will enalize such choises.
      profiti=kron(grid,xplus)-0.2*kron(ones(1,Z),xplus).^1.5;  %x*p-0.2x^1.5
      profiti=profiti-(max(max(Ev))+1)*(kron(grid,x)<0);% I penalize choises corresponing to negative harvest, so it will not be chosen.
      % disp(['The dimensions of ci are ' num2str(size(ci)) ])
      [vrevised(i,:),decision(i,:)]=max(profiti+beta*Ev);
     % disp(['In the loop vreviesed are ' num2str(size(vrevised)) ])
    end
   % disp(['The dimensions of vreviesed are ' num2str(size(vrevised)) ])
     diff=norm(vrevised-vinitial)/norm(vrevised)
     vinitial=vrevised;
    
end

% compute decision rule
vtrue=vrevised;
derule=zeros(N,Z);

for i=1:Z
    derule(:,i)=k(decision(:,i))';
end


plot(k,vrevised(:,[8,11,14]));
%plot(k,vrevised(:,[2,3,4])); %plot for the case of Z=5
xlabel('current stock');
ylabel('value');
title('value of the firm');
legend('p=0.9','p=1','p=1.1')

figure(2);
plot(grid,derule([10,250,501,751,1001],:)','--o');
xlabel('price');
ylabel('stock');
title('next period stock');
legend('stock=1','stock=25','stock=50','stock=75','stock=100')
%%
% part 5
% I will do 10001 simulations
nsim=10001; %number of simulations
nperiods=20; %number of periods
kstartind=N; %index in the stock grid corresponding to the staring stock
pstartind=(Z+1)/2; %index in the price grid corresponding to price 1
result=zeros(nsim,nperiods); %simulations of stock at the end of each period
for i=1:nsim
    kind=kstartind;
    pind=pstartind;
    for j=1:nperiods
        kind=decision(kind,pind);
        result(i,j)=k(kind);
        dist=prob(pind,:); %discreate distribution of future prices given current price  
        [~,~,pind] = histcounts(rand,[0;cumsum(dist(:))/sum(dist)]); % draw from the distribution
    end
    
end
figure(3);
m=sort(result);

plot(1:nperiods,m([round(nsim*0.05),round(nsim*0.95)],:)');
hold on
plot(1:nperiods,mean(m)');
xlabel('periods');
ylabel('stock');
title('dynamics');
legend('5%','95%','mean')
