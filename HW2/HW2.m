%% Home work 2


%% Problem 1
% 
% Please see a pdf with algebraic derivations in the same directory as this
% file
% 


Qa=1/(exp(2)+3)
Qb=1/(exp(2)+3)
Qc=1/(exp(2)+3)
Q0=1/(1+3*exp(-2))

%
%% Problem 2
% I assume that the functions broyden,fdjac,optget,linesearch from the packege compecon are already here.
%result1=broyden(bestresponce,[2,2,2])
bestr = @(x) [(x(1)-1)*(1+exp(-1-x(2))+exp(-1-x(3)))-exp(-1-x(1));(x(2)-1)*(1+exp(-1-x(3))+exp(-1-x(1)))-exp(-1-x(2));(x(3)-1)*(1+exp(-1-x(1))+exp(-1-x(2)))-exp(-1-x(3))] ;
% In all the cases the convergence criterion is max(abs()) of the vector of
% function values. The convergence tolerance is equal to sqrt(eps).
% In all the cases the method converges and the result is
% [1.0985;1.0985;1.0985]
% a) Starting value [1;1;1]
t = cputime;
[x1,fval1,flag,it1,fjacinv]=broyden(bestr,[1;1;1]);
Btime=cputime-t;
%max(abs()) of the vector of function values.
max(abs(fval1))
% b) Starting value [0;0;0]
t = cputime;
[x2,fval2,flag,it2,fjacinv]=broyden(bestr,[0;0;0]);
Btime=[Btime,cputime-t];
%max(abs()) of the vector of function values.
max(abs(fval2))
% c) Starting value [1;1;1]
t = cputime;
[x3,fval3,flag,it3,fjacinv]=broyden(bestr,[0;1;2]);
Btime=[Btime,cputime-t];
%max(abs()) of the vector of function values.
max(abs(fval3))
% d) Starting value [1;1;1]
t = cputime;
[x4,fval4,flag,it4,fjacinv]=broyden(bestr,[3;2;1]);
Btime=[Btime,cputime-t];
%max(abs()) of the vector of function values.
max(abs(fval4))
% Number of iterations in the four cases:
[it1,it2,it3,it4]
% Roots in the four cases:
[x1,x2,x3,x4]
%
%% Problem 3
% Gauss-Sidel procedure
% Please find the code of the function in the corresponding file and the
% theoretical explanations why I use [1,2] interval in the pdf file
t = cputime;
[price,fval,it]=gaussidBestResp([1;1;1],[-1;-1;-1]);
GStime=cputime-t;
msg=sprintf('Initial guess is [1, 1, 1]');
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
msg3=sprintf('Deviations from zero are %d, %d,%d',fval(1),fval(2),fval(3));
disp(msg);disp(msg1);disp(msg2);disp(msg3);

t = cputime;
[price,fval,it]=gaussidBestResp([0;0;0],[-1;-1;-1]);
GStime=[GStime,cputime-t];
msg=sprintf('Initial guess is [0, 0, 0]'); disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
msg3=sprintf('Deviations from zero are %d, %d,%d',fval(1),fval(2),fval(3));
disp(msg1);disp(msg2);disp(msg3);

t = cputime;
[price,fval,it]=gaussidBestResp([0;1;2],[-1;-1;-1]);
GStime=[GStime,cputime-t];
msg=sprintf('Initial guess is [0, 1, 2]');disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
msg3=sprintf('Deviations from zero are %d, %d,%d',fval(1),fval(2),fval(3));
disp(msg1);disp(msg2);disp(msg3);

t = cputime;
[price,fval,it]=gaussidBestResp([3;2;1],[-1;-1;-1]);
GStime=[GStime,cputime-t];
msg=sprintf('Initial guess is [3, 2, 1]');disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
msg3=sprintf('Deviations from zero are %d, %d,%d',fval(1),fval(2),fval(3));
disp(msg1);disp(msg2);disp(msg3);

%Now let's compare the timing
disp('Upper row is Broyden.Lower is Gauss-Sidel');
disp(Btime);
disp(GStime);


%% Problem 4
%Let's define the demand functions
qA = @(p) exp(-1-p(1))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
qB = @(p) exp(-1-p(2))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
qC = @(p) exp(-1-p(3))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
maxit=100;%max number of iterations
tol=sqrt(eps()); %convergence tolerance
price=[1;1;1];
t = cputime;
for it=1:maxit
    newprice=1./(1-[qA(price);qB(price);qC(price)]);
    if max(abs(newprice-price))<tol
           break; %breaking out of loop if the root is found
    end
    price=newprice;
end
price=newprice;
Itime=cputime-t;
msg=sprintf('Initial guess is [1, 1, 1]');disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
disp(msg1);disp(msg2);
price=[0;0;0];
t = cputime;
for it=1:maxit
    newprice=1./(1-[qA(price);qB(price);qC(price)]);
    if max(abs(newprice-price))<tol
           break; %breaking out of loop if the root is found
    end
    price=newprice;
end
price=newprice;
Itime=[Itime,cputime-t];
msg=sprintf('Initial guess is [0, 0, 0]');disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
disp(msg1);disp(msg2);
price=[0;1;2];
t = cputime;
for it=1:maxit
    newprice=1./(1-[qA(price);qB(price);qC(price)]);
    if max(abs(newprice-price))<tol
           break; %breaking out of loop if the root is found
    end
    price=newprice;
end
price=newprice;
Itime=[Itime,cputime-t];
msg=sprintf('Initial guess is [0, 1, 2]');disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
disp(msg1);disp(msg2);
price=[3;2;1];
t = cputime;
for it=1:maxit
    newprice=1./(1-[qA(price);qB(price);qC(price)]);
    if max(abs(newprice-price))<tol
           break; %breaking out of loop if the root is found
    end
    price=newprice;
end
price=newprice;
Itime=[Itime,cputime-t];
msg=sprintf('Initial guess is [3, 2, 1]');disp(msg);
msg1=sprintf('Number of iterations is %d',it);
msg2=sprintf('Equilibrium prices are %d, %d,%d',price(1),price(2),price(3));
disp(msg1);disp(msg2);
%Now let's compare the timing
disp('Upper row is Broyden. Middle is Gauss-Sidel. Lower is Iteration');
disp(Btime);
disp(GStime);
disp(Itime);
%I would say everything works pretty fast
%
%% Problen 5
vc = [-4,-3,-2,-1, 0, 1];
pa=[];
pc=[];
for i=vc   
[price,fval,it]=gaussidBestResp([1;1;1],[-1;-1;i]);
pa=[pa,price(1)];
pc=[pc,price(3)];
end
disp([vc;pa;pc]);
plot(vc,[pa;pc]);
title('Problem 5');
xlabel('Quality of firm C');
ylabel('Prices of goods');
legend('p_A','p_C');
%% Problem 6




