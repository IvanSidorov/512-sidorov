function [price,fval,it]=gaussidBestResp(initprice,v) 
% Gauss-Sidel method for best responce system of equations.
% Please find the theoretical explanations why I use [1,2] interval in the pdf file
tol=sqrt(eps()); %convergence tolerance
it=0; %number of iterations
maxit=100; %max number of iterations of the main loop
maxsecit=1000; %max number of iterations of the secant loop
%Best responce functions of players A,B and C:
bestrA = @(pa,pb,pc) (pa-1)*(1+exp(v(2)-pb)+exp(v(3)-pc))-exp(v(1)-pa);
bestrB = @(pa,pb,pc) (pb-1)*(1+exp(v(3)-pc)+exp(v(1)-pa))-exp(v(2)-pb);
bestrC = @(pa,pb,pc) (pc-1)*(1+exp(v(1)-pa)+exp(v(2)-pb))-exp(v(3)-pc);
price=initprice;
oldprice=initprice;
for it=1:maxit
   %setting left and right points for secant method
   left=1;
   right=max(2,v(1));
   old=left;% let left point be initial guess of secant method 
   %calulating function values at the end points
   lval=bestrA(left,price(2),price(3));
   rval=bestrA(right,price(2),price(3));
   for i=1:maxsecit
       %calculating the new point
       new=(left*rval-right*lval)/(rval-lval);
       %calculating value in the new point
       nval=bestrA(new,price(2),price(3));
       if max(abs(old-new),abs(nval))<tol
           break; %breaking out of loop if the root is found
       end
       if sign(nval)*sign(lval)<0
           right=new;
           rval=nval;
       else
           left=new;
           lval=nval;
       end       
       old=new; %preserving previous guess 
   end 
   if i==maxsecit
       msg=sprintf('Secant for A: function value:%d,left point:%d, left value:%d,right point:%d,right value:%d',nval,left,lval,right,rval);disp(msg);
   end
   price=[new;price(2);price(3)];
   

   %setting left and right points for secant method
   left=1;
   right=max(2,v(2));
   %calulating function values at the end points
   lval=bestrB(price(1),left,price(3));
   rval=bestrB(price(1),right,price(3));
   for i=1:maxsecit
       %calculating the new point
       new=(left*rval-right*lval)/(rval-lval);
       %calculating value in the new point
       nval=bestrB(price(1),new,price(3));
       if max(abs(old-new),abs(nval))<tol
           break; %breaking out of loop if the root is found
       end
       if sign(nval)*sign(lval)<0
           right=new;
           rval=nval;
       else
           left=new;
           lval=nval;
       end 
       old=new; %preserving previous guess 
   end 
   if i==maxsecit
       msg=sprintf('Secant for B: function value:%d,left point:%d, left value:%d,right point:%d,right value:%d',nval,left,lval,right,rval);disp(msg);
   end
   price=[price(1);new;price(3)];
   
   %setting left and right points for secant method
   left=1;
   right=max(2,v(3));
   %calulating function values at the end points
   lval=bestrC(price(1),price(2),left);
   rval=bestrC(price(1),price(2),right);
   for i=1:maxsecit
       %calculating the new point
       new=(left*rval-right*lval)/(rval-lval);
       %calculating value in the new point
       nval=bestrC(price(1),price(2),new);
       if max(abs(old-new),abs(nval))<tol
           break; %breaking out of loop if the root is found
       end
       if sign(nval)*sign(lval)<0
           right=new;
           rval=nval;
       else
           left=new;
           lval=nval;
       end   
       old=new; %preserving previous guess 
   end 
   if i==maxsecit
       msg=sprintf('Secant for B: function value:%d,left point:%d, left value:%d,right point:%d,right value:%d',nval,left,lval,right,rval);disp(msg);
       
   end
   price=[price(1);price(2);new];
   fval = [bestrA(price(1),price(2),price(3)),bestrB(price(1),price(2),price(3)),bestrC(price(1),price(2),price(3))];
   fnorm = norm(fval,inf);%deviations of the functions from zero
   vnorm = norm(price-oldprice,inf);%the size of the step in the big loop
   if max(fnorm,vnorm)<tol, break; end 
   oldprice=price;
end

end