function [f,df] = LnL(b)
load('hw3.mat');

f = -sum(-exp(X*b)+diag(y)*(X*b)-log(factorial(y))); %negative for use in fminunc
df = -sum(-diag(exp(X*b))*X+diag(y)*X);
end