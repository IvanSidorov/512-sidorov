load('hw3.mat');
b0=[0;0;0;0;0;0];
lnL = @(X,y,b) sum(-exp(X*b)+y.*(X*b)-log(factorial(y)));
% don't need the last part of loglik
%dfu=@(b) -sum(-diag(exp(X*b))*X+diag(y)*X);

fun = @(b) -lnL(X,y,b); % negative of the likelihood function for use in fminunc 
options = optimoptions('fminunc','Display','off','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');

[b,fval,exitflag,output,grad,hessian] =  fminunc(fun,b0,options);

[bNM,fvalNM,exitflagNM,outputNM]  = fminsearch(fun,b0);
% here you should have watched output, default MaxFunEval is not enough,
% need to increase it and you get the same answer.

options = optimoptions('fminunc','Display','off','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');

fund= @LnL;% negative of the likelihood function and it's derivative for use in fminunc 

[bd,fvald,exitflagd,outputd,gradd,hessiand] =  fminunc(fund,b0,options);
%% BHHH method
score=@(b) -diag(exp(X*b))*X+diag(y)*X;

maxit=1000;%max number of iterations
tol=sqrt(eps()); %convergence tolerance
bh=b0; % initial point
    
for it=1:maxit
    s=score(bh);
    step=(s'*s)\(s'*ones(length(s),1));
    bh=bh+step;    
    if max(abs(step))<tol
           break; %breaking out of loop if the optimum is found
    end
    
end
Hessinit=score(b0)'*score(b0)/6; %initial Hessian
Hessfin=score(bh)'*score(bh)/6; %final Hessian
disp('Parameters:');
disp(['Nelder-Mead ','FMINUNC w/o ','FMINUNC w/  ','BHHH']);
disp([bNM,b,bd,bh]);
disp('Number of iterations:');
disp(['Nelder-Mead ','FMINUNC w/o ','FMINUNC w/  ','BHHH']);
disp([outputNM.iterations,output.iterations,outputd.iterations,it]);
disp('Number of function evaluations:');
disp(['Nelder-Mead ','FMINUNC w/o ','FMINUNC w/  ','BHHH']);
disp([outputNM.funcCount,output.funcCount,outputd.funcCount,it]);
disp('Function values:');
disp(['Nelder-Mead ','FMINUNC w/o ','FMINUNC w/  ','BHHH']);
disp([lnL(X,y,bNM),lnL(X,y,b),lnL(X,y,bd),lnL(X,y,bh)]);
    
    %[dfu(bNM)',dfu(b)',dfu(bd)',dfu(bh)']
%%   
eiginit=eig(Hessinit)%eigenvalues of initial Hessian
eigfin=eig(Hessfin)%eigenvalues of final Hessian

%% NLLS method
Jacob=@(b) -diag(exp(X*b))*X;

maxit=100;%max number of iterations
tol=sqrt(eps()); %convergence tolerance
bj=b0; % initial point
    
for it=1:maxit
    j=Jacob(bj);
    res=y-exp(X*bj);
    step=-(j'*j)\(j'*(res));
    bj=bj+step;
    if max(abs(step))<tol
           break; %breaking out of loop if the optimum is found
    end
    
end
disp('NLLS');
disp('Parameters:');
disp(bj);
disp('Number of iterations:');
disp(it);
disp('Number of function evaluations:');
disp(2*it); %I'm not completely sure what numbet is approptiate hear, 
%but I would say that for each iteration we need to calulate j and res, and each
%of them is compareable to calculating  the likelihood function.
disp('Function value:');
disp(lnL(X,y,bj));
